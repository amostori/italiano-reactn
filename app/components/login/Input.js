import React from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  view: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    width: '$screenWidth',
    justifyContent: 'space-between',
  },
  input: {
    height: 40,
    marginRight: 20,
    width: 150,
    borderRadius: 5,
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
  },
  text: {
    color: 'white',
    lineHeight: 30,
    paddingLeft: 20,
    fontSize: 25,
  },
});

const Input = ({
  label, onChangeText, value, placeholder, secureTextEntry,
}) => {
  const { view, input, text } = styles;

  return (
    <View style={view}>
      <Text style={text}>{label}</Text>
      <TextInput
        secureTextEntry={secureTextEntry}
        value={value}
        onChangeText={onChangeText}
        style={input}
        placeholder={placeholder}
      />
    </View>
  );
};
Input.propTypes = {
  label: PropTypes.string,
  onChangeText: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  secureTextEntry: PropTypes.bool,
};

export default Input;
