import React from 'react';
import {
  View, TouchableOpacity, Text, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    width: 200,
    height: 70,
  },
  wrapper: {},
  tekst: { color: '#ffffff', fontSize: 40 },
});

const MyButton = ({ onPress, tekst }) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <View style={styles.wrapper}>
      <Text style={styles.tekst}>{tekst}</Text>
    </View>
  </TouchableOpacity>
);

MyButton.propTypes = {
  onPress: PropTypes.func,
  tekst: PropTypes.string,
};

export default MyButton;
