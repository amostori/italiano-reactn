import { createStackNavigator } from 'react-navigation';
import Home from '../screens/Home';
import LoginForm from '../components/login/LoginForm';

const RootStack = createStackNavigator(
  {
    LoginForm,
  },

  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#3b5998',
      },
      headerTitleStyle: {
        fontWeight: 'bold',
        color: 'white',
      },
    },
  },
);
export default RootStack;
