import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import Container from '../components/containers/Container';
import MainPage from './MainPage';

export default class Home extends Component {
  static navigationOptions = {
    title: 'Italiano Vero',
  };

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#192f6a" translucent={false} barStyle="light-content" />
        <MainPage />
      </Container>
    );
  }
}
