import React, { Component } from 'react';
import { View, Text, Dimensions, } from 'react-native';
import podmiots from './data/podmiots';
import tenses from './data/tenses';
import words from './data/words';
import MyButton from '../components/buttons/MyButton';
import EStyleSheet from 'react-native-extended-stylesheet';


export default class MainPage extends Component {
  state = {
    podmiot: 'Io',
    tense: 'Presente',
    word: 'Ascoltare',
  };

  handleOnPodmiot = () => {
    const item = podmiots[Math.floor(Math.random() * podmiots.length)];
    this.setState({
      podmiot: item,
    });
  };

  handleOnTense = () => {
    const item = tenses[Math.floor(Math.random() * tenses.length)];
    this.setState({
      tense: item,
    });
  };

  handleOnWord = () => {
    const item = words[Math.floor(Math.random() * words.length)];
    this.setState({
      word: item,
    });
  };

  render() {
    const { podmiot, tense, word } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.view1}>
            <Text style={styles.podmiot}>{podmiot}</Text>
        </View>
        <View style={styles.view2}>
            <Text style={styles.verbo}>{word}</Text>
            <Text style={styles.czas}>{tense}</Text>
        </View>
        <View style={styles.view3}>
            <MyButton tekst="Verbo" onPress={this.handleOnWord} />
            <MyButton tekst="Czas" onPress={this.handleOnTense} />
            <MyButton tekst="Chi?" onPress={this.handleOnPodmiot} />
        </View>
      </View>
    );
  }

}
      const styles = EStyleSheet.create({
        $screenWidth: Dimensions.get('window').width,
        container: {
            flex: 1,
            width: '$screenWidth',
            justifyContent: 'space-around',
            alignItems: 'center',
          },
          view1: {
            width: '$screenWidth',
              justifyContent: 'center',
              alignItems: 'center',
          },
          podmiot: {
              fontSize: 50,
              color: '$whiteColor',
          },
          view2: {
            width: '$screenWidth',
            marginLeft: 50,
              justifyContent: 'flex-start',
          },
          verbo: {
              marginTop: 10,
            fontSize: 20,
            color: '$whiteColor',
        },
          czas: {
              marginTop: 20,
            fontSize: 20,
            fontStyle: 'italic',
            color: '$whiteColor',
        },
          view3: {
            justifyContent: 'space-around',
            flexDirection: 'row',
            width: '$screenWidth',
            alignItems: 'flex-end',
          },
        });
      