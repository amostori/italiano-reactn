import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from '@firebase/app';
import '@firebase/auth';
import EStyleSheet from 'react-native-extended-stylesheet';
import Container from '../containers/Container';
import ButtonLogin from '../buttons/ButtonLogin';
import Input from './Input';
import Spinner from '../spinner/Spinner';

const styles = EStyleSheet.create({
  errorTextStyle: {
    fontSize: 30,
    color: 'red',
    alignSelf: 'center',
  },
});

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false,
  };

  handleOnLogin = () => {
    const { email, password } = this.state;
    this.setState({ error: '', loading: true });
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(this.onLoginSucces)
      .catch(() => {
        firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then(this.onLoginSucces)
          .catch(this.onLoginFailed);
      });
  };

  onLoginFailed = () => {
    this.setState({
      error: 'Autentication failed',
      loading: false,
    });
  };

  onLoginSucces = () => {
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: '',
    });
  };

  renderButton = () => {
    const { loading } = this.state;
    if (loading) {
      return <Spinner size="small" />;
    }
    return <ButtonLogin onPress={this.handleOnLogin} tekst="Login" />;
  };

  render() {
    const { email, password, error } = this.state;
    return (
      <Container>
        <Input
          value={email}
          placeholder="email"
          label="Email"
          onChangeText={tekst => this.setState({ email: tekst })}
        >
          {email}
        </Input>
        <Input
          value={password}
          secureTextEntry
          label="Password"
          placeholder="password"
          onChangeText={tekst => this.setState({ password: tekst })}
        >
          {password}
        </Input>
        <Text style={styles.errorTextStyle}>{error}</Text>
        {this.renderButton}
      </Container>
    );
  }
}

export default LoginForm;
