import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
  },
});

const Container = ({ children }) => (
  <LinearGradient colors={['#192f6a', '#3b5998', '#4c669f']} style={styles.linearGradient}>
    <View style={styles.container}>{children}</View>
  </LinearGradient>
);

Container.propTypes = {
  children: PropTypes.any,
};

export default Container;
