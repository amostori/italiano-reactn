import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

const styles = {
  spinner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const Spinner = ({ size }) => (
  <View style={styles.spinner}>
    <ActivityIndicator size={size || 'large'} />
  </View>
);

Spinner.propTypes = {
  size: PropTypes.string,
};

export default Spinner;
