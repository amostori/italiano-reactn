import React from 'react';
import { Dimensions, View } from 'react-native';
import firebase from '@firebase/app';
import '@firebase/auth';
import EStyleSheet from 'react-native-extended-stylesheet';
import { createAppContainer } from 'react-navigation';
import Navigator from './app/config/route';
import LoginForm from './app/components/login/LoginForm';
import ButtonLogin from './app/components/buttons/ButtonLogin';
import Spinner from './app/components/spinner/Spinner';

const AppContainer = createAppContainer(Navigator);

EStyleSheet.build({
  // always call EStyleSheet.build() even if you don't use global variables!
  $screenWidth: Dimensions.get('window').width,
  $textColor: '#0275d8',
  $whiteColor: '#fff',
  $firstBlue: '#192f6a',
  $secondBlue: '#3b5998',
  $thirdBlue: '#4c669f',
  // $outline: 1,
});

class App extends React.Component {
  state = {
    loggedIn: null,
  };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCp3MqNqJbNM4DxrcMF47ZIRYHQrRg2BNE',
      authDomain: 'italiano-b2541.firebaseapp.com',
      databaseURL: 'https://italiano-b2541.firebaseio.com',
      projectId: 'italiano-b2541',
      storageBucket: 'italiano-b2541.appspot.com',
      messagingSenderId: '425528374043',
    });
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  handleLogOut = () => {
    firebase.auth().signOut();
  };

  renderContent = () => {
    const { loggedIn } = this.state;

    switch (loggedIn) {
      case true:
        return <ButtonLogin onPress={this.handleLogOut}>Log out</ButtonLogin>;
      case false:
        return <LoginForm />;
      default:
        return <Spinner size="large" />;
    }
  };

  render() {
    return <View>{this.renderContent()}</View>;
  }
}

export default App;
