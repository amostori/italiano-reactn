export default [
  'Presente',
  'Imperfetto',
  'Futuro',
  'Condizionale',
  'Congiuntivo presente',
  'Congiuntivo imperfetto',
];
